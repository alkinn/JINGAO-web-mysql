package com.erqi;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
@SpringBootApplication
@EnableScheduling
@MapperScan("com.erqi.mapper")
public class ErqiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ErqiApplication.class, args);
    }

}
