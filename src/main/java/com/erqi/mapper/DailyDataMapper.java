package com.erqi.mapper;

import com.erqi.pojo.DailyData;
import com.erqi.pojo.DailyDataExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
@Mapper
public interface DailyDataMapper {
    long countByExample(DailyDataExample example);

    int deleteByExample(DailyDataExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(DailyData record);

    int insertSelective(DailyData record);

    List<DailyData> selectByExample(DailyDataExample example);

    DailyData selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") DailyData record, @Param("example") DailyDataExample example);

    int updateByExample(@Param("record") DailyData record, @Param("example") DailyDataExample example);

    int updateByPrimaryKeySelective(DailyData record);

    int updateByPrimaryKey(DailyData record);
}