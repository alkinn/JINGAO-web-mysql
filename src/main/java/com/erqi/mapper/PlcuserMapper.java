package com.erqi.mapper;

import com.erqi.pojo.Plcuser;
import com.erqi.pojo.PlcuserExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

@Component
@Mapper
public interface PlcuserMapper {
    long countByExample(PlcuserExample example);

    int deleteByExample(PlcuserExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Plcuser record);

    int insertSelective(Plcuser record);

    List<Plcuser> selectByExample(PlcuserExample example);

    Plcuser selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Plcuser record, @Param("example") PlcuserExample example);

    int updateByExample(@Param("record") Plcuser record, @Param("example") PlcuserExample example);

    int updateByPrimaryKeySelective(Plcuser record);

    int updateByPrimaryKey(Plcuser record);

    //自定义 start
    Plcuser selectByUserName(String username);
    //自定义 end

}