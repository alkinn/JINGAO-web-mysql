package com.erqi.mapper;

import com.erqi.pojo.Qrandplc;
import com.erqi.pojo.QrandplcExample;

import java.util.LinkedHashMap;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

@Component
@Mapper
public interface QrandplcMapper {
    long countByExample(QrandplcExample example);

    int deleteByExample(QrandplcExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Qrandplc record);

    int     insertSelective(Qrandplc record);

    List<Qrandplc> selectByExample(QrandplcExample example);

    Qrandplc selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Qrandplc record, @Param("example") QrandplcExample example);

    int updateByExample(@Param("record") Qrandplc record, @Param("example") QrandplcExample example);

    int updateByPrimaryKeySelective(Qrandplc record);

    int updateByPrimaryKey(Qrandplc record);

    /*非原生*/
    Integer selectByQrandplc(Qrandplc record);
    List<Qrandplc> selectExample(QrandplcExample example);
    List<Qrandplc> selectAll();
    Qrandplc selectByPrimaryQRcode(String QRcode);
    List<Qrandplc> NEWselectByExample(QrandplcExample example);
    List<LinkedHashMap<String, Object>>  SelectDYI(String sql);
    String selectMonitor();

}