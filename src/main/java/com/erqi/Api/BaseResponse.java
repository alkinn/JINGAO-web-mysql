package com.erqi.Api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author king
 * @Date 2020/6/8 13:23
 * @Version 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseResponse<T> {

    /**
     * 0 没问题
     * 1 数据缺失，
     * 2 数据错误。
     * 3 数据为空
     * 407 无权限
     * 401 未登录
     * 200 成功不管什么
     * 202 数据为空
     * 405 通用失败
     * 406 旧密码不匹配
     */
    private Integer code;

    /**
     * 成功状态吗；
     */
    private String msg;
    /**、
     * 下面data的长度。 获取所有数据分也时显示的所有的数据。曲线图是是下面的长度
     */
    private Integer count;
    /**
     * 当前数据
     */
    private T data;
}
