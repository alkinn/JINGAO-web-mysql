package com.erqi.configs.Shiro;


import com.erqi.mapper.PlcuserMapper;
import com.erqi.pojo.Plcuser;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Author king
 * @Date 2020/7/7 13:01
 * @Version 1.0
 */


//自定义的userRealm
public class UserRealm extends AuthorizingRealm {


    @Autowired
    PlcuserMapper userMapper;




    //授权
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {

        System.out.println("执行了 授权>>>>doGetAuthorizationInfo");

        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        //拿到当前用户对象。
        Subject subject = SecurityUtils.getSubject();
        //拿到当前登录用对象的user
        Plcuser user = (Plcuser) subject.getPrincipal();
        info.addStringPermission(user.getPrams());
        info.addRole(user.getRoles());

        return info;
    }


    //认证
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        System.out.println("执行了 认证>>>>doGetAuthorizationInfo");
        //获取当前登录对象
        UsernamePasswordToken userToken = (UsernamePasswordToken) token;

        //从数据库中取
        Plcuser plcuser = userMapper.selectByUserName(userToken.getUsername());

        if (null==plcuser){
            return null;
        }

        Subject currentSubject = SecurityUtils.getSubject();
        Session session = currentSubject.getSession();
        session.setAttribute("loginUser",plcuser);
        return new SimpleAuthenticationInfo(plcuser,plcuser.getPassword(),"");
    }
}
