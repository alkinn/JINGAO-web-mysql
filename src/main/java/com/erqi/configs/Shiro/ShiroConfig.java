package com.erqi.configs.Shiro;

import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @Author king
 * @Date 2020/7/7 11:39
 * @Version 1.0
 */
@Configuration
public class ShiroConfig {

    //shiroFilterFactoryBean   shiro的拦截器工厂bean

    @Bean
    public ShiroFilterFactoryBean getShiroFilterFactoryBean(@Qualifier("getDefaultWebSecurityManager") DefaultWebSecurityManager defaultWebSecurityManager){
        ShiroFilterFactoryBean bean = new ShiroFilterFactoryBean();
        //设置安全管理器，
        bean.setSecurityManager(defaultWebSecurityManager);
        /*
        *  anon  ：无需认证就可以访问，
        *  authc : 必须认证了才能访问，
        *  user  ： 必须拥有 记住我 功能才能用。
        *  perms ： 拥有对某个资源的权限。
        *  roles ： 拥有谋和角色才能访问。
        *  */
        //配置自定义拦截器
        Map<String, Filter> FilterMaps = new LinkedHashMap<>();
        FilterMaps.put("roles",new RoleFilter());
        //配置自定义设置
        Map<String, String> FilterMap = new LinkedHashMap<>();



        FilterMap.put("/api/shrio/dellist","roles[leader,admin]");
        FilterMap.put("/api/shrio/setData","roles[leader,admin]");
        FilterMap.put("/api/shrio/modifyData","roles[leader,admin]");
        FilterMap.put("/api/shrio/GetPermissions","roles[leader,admin]");

        FilterMap.put("/user/getAllUser","roles[leader,admin]");
        FilterMap.put("/user/saveUser","roles[leader,admin]");
        FilterMap.put("/user/delUser","roles[leader,admin]");
        FilterMap.put("/user/modifyUser","roles[leader,admin]");
        FilterMap.put("/user/GetPermissions","roles[leader,admin]");

        FilterMap.put("/api/shrio/*","authc");
        FilterMap.put("/user/*","authc");
        //设置登出
        FilterMap.put("/logout", "logout");
        bean.setFilters(FilterMaps);
        bean.setFilterChainDefinitionMap(FilterMap); //加载自定义设置
        //设置登录请求
        bean.setLoginUrl("/nologin");
        //设置未授权页面
        bean.setUnauthorizedUrl("/noauth");


        return bean;
    }


    //DefaultWebSecurityManager  //web安全管理器，
    @Bean
    public DefaultWebSecurityManager getDefaultWebSecurityManager(@Qualifier("userRealm") UserRealm userRealm){
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        //需要关联userrealm
        securityManager.setRealm(userRealm);

        return securityManager;
    }

    //创建 realm 对象 ， 需要自定义类。1

    @Bean
    public UserRealm userRealm(){


        return  new UserRealm();
    }
}
