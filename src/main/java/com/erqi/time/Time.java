package com.erqi.time;


import com.erqi.dao.impl.plcDaoImpl;
import com.erqi.dao.plcDao;
import com.erqi.mapper.DailyDataMapper;
import com.erqi.pojo.DailyData;
import com.erqi.pojo.Qrandplc;
import com.erqi.uitl.dates;
import com.github.s7connector.api.DaveArea;
import com.github.s7connector.api.S7Connector;
import com.github.s7connector.api.factory.S7ConnectorFactory;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @Author king
 * @Date 2020/6/1 21:17
 * @Version 1.0
 */
@Configuration
public class Time {
    public static Logger log = Logger.getLogger(Time.class);



    @Autowired
    plcDaoImpl qrMapper;

    @Autowired
    plcDao plc;

    @Autowired
    DailyDataMapper dataMapper;




    /**
     * 每天的0点1分开始执行  先获取long 数组0开方机1一体机3所有
     */
   @Scheduled(cron = "0 1 0 * * ?")
    //@Scheduled(fixedDelay = 20000)
    public void plcDataToDailyData() {
        log.info("执行定时任务");
        long[] kai = plc.getKai();
        /*for (long s :
                kai) {
            System.out.println("s = " + s);
        }*/
        DailyData dailyData= new DailyData(dates.getPastDate(1,0),(int)kai[0],(int)kai[1],(int)kai[2]);
        dailyData.setK1((int) qrMapper.getToday1K());
        dailyData.setT1((int) qrMapper.getToday1T());
        dailyData.setK2((int) qrMapper.getToday2K());
        dailyData.setT2((int) qrMapper.getToday2T());
        dailyData.setK3((int) qrMapper.getToday3K());
        dailyData.setT3((int) qrMapper.getToday3T());
        dailyData.setK4((int) qrMapper.getToday4K());
        dailyData.setT4((int) qrMapper.getToday4T());
        dataMapper.insert(dailyData);

        log.info("完成");
    }

}
