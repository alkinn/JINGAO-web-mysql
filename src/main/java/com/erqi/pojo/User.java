package com.erqi.pojo;

import lombok.Data;
import lombok.ToString;

/**
 * @Author king
 * @Date 2020/7/22 15:43
 * @Version 1.0
 */
@Data
@ToString
public class User {
    String username;
    String password;
}
