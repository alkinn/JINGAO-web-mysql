package com.erqi.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

@ToString
@NoArgsConstructor
@Data
public class DailyData {
    public DailyData(Date date, Integer kaifang, Integer yiti, Integer alls) {
        this.date = date;
        this.kaifang = kaifang;
        this.yiti = yiti;
        this.alls = alls;
    }

    private Integer id;
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date date;

    private Integer kaifang;

    private Integer yiti;

    private Integer alls;

    private Integer k1;

    private Integer t1;

    private Integer k2;

    private Integer t2;

    private Integer k3;

    private Integer t3;

    private Integer k4;

    private Integer t4;


}

