package com.erqi.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class QrandplcExample {
    protected String orderByClause;

    protected boolean distinct;

    protected Integer leftLimit;

    protected Integer limitSize;
    protected Integer page;

    protected Integer limit;

    public Integer getLeftLimit() {
        return leftLimit;
    }

    public void setLeftLimit(Integer leftLimit) {
        this.leftLimit = leftLimit;
    }

    public Integer getLimitSize() {
        return limitSize;
    }

    public void setLimitSize(Integer limitSize) {
        this.limitSize = limitSize;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public void setOredCriteria(List<Criteria> oredCriteria) {
        this.oredCriteria = oredCriteria;
    }

    protected List<Criteria> oredCriteria;

    public QrandplcExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andDateIsNull() {
            addCriterion("Date is null");
            return (Criteria) this;
        }

        public Criteria andDateIsNotNull() {
            addCriterion("Date is not null");
            return (Criteria) this;
        }

        public Criteria andDateEqualTo(Date value) {
            addCriterion("Date =", value, "date");
            return (Criteria) this;
        }

        public Criteria andDateNotEqualTo(Date value) {
            addCriterion("Date <>", value, "date");
            return (Criteria) this;
        }

        public Criteria andDateGreaterThan(Date value) {
            addCriterion("Date >", value, "date");
            return (Criteria) this;
        }

        public Criteria andDateGreaterThanOrEqualTo(Date value) {
            addCriterion("Date >=", value, "date");
            return (Criteria) this;
        }

        public Criteria andDateLessThan(Date value) {
            addCriterion("Date <", value, "date");
            return (Criteria) this;
        }

        public Criteria andDateLessThanOrEqualTo(Date value) {
            addCriterion("Date <=", value, "date");
            return (Criteria) this;
        }

        public Criteria andDateIn(List<Date> values) {
            addCriterion("Date in", values, "date");
            return (Criteria) this;
        }

        public Criteria andDateNotIn(List<Date> values) {
            addCriterion("Date not in", values, "date");
            return (Criteria) this;
        }

        public Criteria andDateBetween(Date value1, Date value2) {
            addCriterion("Date between", value1, value2, "date");
            return (Criteria) this;
        }

        public Criteria andDateNotBetween(Date value1, Date value2) {
            addCriterion("Date not between", value1, value2, "date");
            return (Criteria) this;
        }

        public Criteria andTypeIsNull() {
            addCriterion("type is null");
            return (Criteria) this;
        }

        public Criteria andTypeIsNotNull() {
            addCriterion("type is not null");
            return (Criteria) this;
        }

        public Criteria andTypeEqualTo(String value) {
            addCriterion("type =", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotEqualTo(String value) {
            addCriterion("type <>", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThan(String value) {
            addCriterion("type >", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThanOrEqualTo(String value) {
            addCriterion("type >=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThan(String value) {
            addCriterion("type <", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThanOrEqualTo(String value) {
            addCriterion("type <=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLike(String value) {
            addCriterion("type like", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotLike(String value) {
            addCriterion("type not like", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeIn(List<String> values) {
            addCriterion("type in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotIn(List<String> values) {
            addCriterion("type not in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeBetween(String value1, String value2) {
            addCriterion("type between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotBetween(String value1, String value2) {
            addCriterion("type not between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andQrcodeIsNull() {
            addCriterion("QRcode is null");
            return (Criteria) this;
        }

        public Criteria andQrcodeIsNotNull() {
            addCriterion("QRcode is not null");
            return (Criteria) this;
        }

        public Criteria andQrcodeEqualTo(String value) {
            addCriterion("QRcode =", value, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeNotEqualTo(String value) {
            addCriterion("QRcode <>", value, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeGreaterThan(String value) {
            addCriterion("QRcode >", value, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeGreaterThanOrEqualTo(String value) {
            addCriterion("QRcode >=", value, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeLessThan(String value) {
            addCriterion("QRcode <", value, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeLessThanOrEqualTo(String value) {
            addCriterion("QRcode <=", value, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeLike(String value) {
            addCriterion("QRcode like", value, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeNotLike(String value) {
            addCriterion("QRcode not like", value, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeIn(List<String> values) {
            addCriterion("QRcode in", values, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeNotIn(List<String> values) {
            addCriterion("QRcode not in", values, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeBetween(String value1, String value2) {
            addCriterion("QRcode between", value1, value2, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeNotBetween(String value1, String value2) {
            addCriterion("QRcode not between", value1, value2, "qrcode");
            return (Criteria) this;
        }

        public Criteria andNumRobotIsNull() {
            addCriterion("Num_robot is null");
            return (Criteria) this;
        }

        public Criteria andNumRobotIsNotNull() {
            addCriterion("Num_robot is not null");
            return (Criteria) this;
        }

        public Criteria andNumRobotEqualTo(String value) {
            addCriterion("Num_robot =", value, "numRobot");
            return (Criteria) this;
        }

        public Criteria andNumRobotNotEqualTo(String value) {
            addCriterion("Num_robot <>", value, "numRobot");
            return (Criteria) this;
        }

        public Criteria andNumRobotGreaterThan(String value) {
            addCriterion("Num_robot >", value, "numRobot");
            return (Criteria) this;
        }

        public Criteria andNumRobotGreaterThanOrEqualTo(String value) {
            addCriterion("Num_robot >=", value, "numRobot");
            return (Criteria) this;
        }

        public Criteria andNumRobotLessThan(String value) {
            addCriterion("Num_robot <", value, "numRobot");
            return (Criteria) this;
        }

        public Criteria andNumRobotLessThanOrEqualTo(String value) {
            addCriterion("Num_robot <=", value, "numRobot");
            return (Criteria) this;
        }

        public Criteria andNumRobotLike(String value) {
            addCriterion("Num_robot like", value, "numRobot");
            return (Criteria) this;
        }

        public Criteria andNumRobotNotLike(String value) {
            addCriterion("Num_robot not like", value, "numRobot");
            return (Criteria) this;
        }

        public Criteria andNumRobotIn(List<String> values) {
            addCriterion("Num_robot in", values, "numRobot");
            return (Criteria) this;
        }

        public Criteria andNumRobotNotIn(List<String> values) {
            addCriterion("Num_robot not in", values, "numRobot");
            return (Criteria) this;
        }

        public Criteria andNumRobotBetween(String value1, String value2) {
            addCriterion("Num_robot between", value1, value2, "numRobot");
            return (Criteria) this;
        }

        public Criteria andNumRobotNotBetween(String value1, String value2) {
            addCriterion("Num_robot not between", value1, value2, "numRobot");
            return (Criteria) this;
        }

        public Criteria andNumMachineIsNull() {
            addCriterion("Num_machine is null");
            return (Criteria) this;
        }

        public Criteria andNumMachineIsNotNull() {
            addCriterion("Num_machine is not null");
            return (Criteria) this;
        }

        public Criteria andNumMachineEqualTo(String value) {
            addCriterion("Num_machine =", value, "numMachine");
            return (Criteria) this;
        }

        public Criteria andNumMachineNotEqualTo(String value) {
            addCriterion("Num_machine <>", value, "numMachine");
            return (Criteria) this;
        }

        public Criteria andNumMachineGreaterThan(String value) {
            addCriterion("Num_machine >", value, "numMachine");
            return (Criteria) this;
        }

        public Criteria andNumMachineGreaterThanOrEqualTo(String value) {
            addCriterion("Num_machine >=", value, "numMachine");
            return (Criteria) this;
        }

        public Criteria andNumMachineLessThan(String value) {
            addCriterion("Num_machine <", value, "numMachine");
            return (Criteria) this;
        }

        public Criteria andNumMachineLessThanOrEqualTo(String value) {
            addCriterion("Num_machine <=", value, "numMachine");
            return (Criteria) this;
        }

        public Criteria andNumMachineLike(String value) {
            addCriterion("Num_machine like", value, "numMachine");
            return (Criteria) this;
        }

        public Criteria andNumMachineNotLike(String value) {
            addCriterion("Num_machine not like", value, "numMachine");
            return (Criteria) this;
        }

        public Criteria andNumMachineIn(List<String> values) {
            addCriterion("Num_machine in", values, "numMachine");
            return (Criteria) this;
        }

        public Criteria andNumMachineNotIn(List<String> values) {
            addCriterion("Num_machine not in", values, "numMachine");
            return (Criteria) this;
        }

        public Criteria andNumMachineBetween(String value1, String value2) {
            addCriterion("Num_machine between", value1, value2, "numMachine");
            return (Criteria) this;
        }

        public Criteria andNumMachineNotBetween(String value1, String value2) {
            addCriterion("Num_machine not between", value1, value2, "numMachine");
            return (Criteria) this;
        }

        public Criteria andState1IsNull() {
            addCriterion("state1 is null");
            return (Criteria) this;
        }

        public Criteria andState1IsNotNull() {
            addCriterion("state1 is not null");
            return (Criteria) this;
        }

        public Criteria andState1EqualTo(String value) {
            addCriterion("state1 =", value, "state1");
            return (Criteria) this;
        }

        public Criteria andState1NotEqualTo(String value) {
            addCriterion("state1 <>", value, "state1");
            return (Criteria) this;
        }

        public Criteria andState1GreaterThan(String value) {
            addCriterion("state1 >", value, "state1");
            return (Criteria) this;
        }

        public Criteria andState1GreaterThanOrEqualTo(String value) {
            addCriterion("state1 >=", value, "state1");
            return (Criteria) this;
        }

        public Criteria andState1LessThan(String value) {
            addCriterion("state1 <", value, "state1");
            return (Criteria) this;
        }

        public Criteria andState1LessThanOrEqualTo(String value) {
            addCriterion("state1 <=", value, "state1");
            return (Criteria) this;
        }

        public Criteria andState1Like(String value) {
            addCriterion("state1 like", value, "state1");
            return (Criteria) this;
        }

        public Criteria andState1NotLike(String value) {
            addCriterion("state1 not like", value, "state1");
            return (Criteria) this;
        }

        public Criteria andState1In(List<String> values) {
            addCriterion("state1 in", values, "state1");
            return (Criteria) this;
        }

        public Criteria andState1NotIn(List<String> values) {
            addCriterion("state1 not in", values, "state1");
            return (Criteria) this;
        }

        public Criteria andState1Between(String value1, String value2) {
            addCriterion("state1 between", value1, value2, "state1");
            return (Criteria) this;
        }

        public Criteria andState1NotBetween(String value1, String value2) {
            addCriterion("state1 not between", value1, value2, "state1");
            return (Criteria) this;
        }

        public Criteria andWorkshopIsNull() {
            addCriterion("workshop is null");
            return (Criteria) this;
        }

        public Criteria andWorkshopIsNotNull() {
            addCriterion("workshop is not null");
            return (Criteria) this;
        }

        public Criteria andWorkshopEqualTo(String value) {
            addCriterion("workshop =", value, "workshop");
            return (Criteria) this;
        }

        public Criteria andWorkshopNotEqualTo(String value) {
            addCriterion("workshop <>", value, "workshop");
            return (Criteria) this;
        }

        public Criteria andWorkshopGreaterThan(String value) {
            addCriterion("workshop >", value, "workshop");
            return (Criteria) this;
        }

        public Criteria andWorkshopGreaterThanOrEqualTo(String value) {
            addCriterion("workshop >=", value, "workshop");
            return (Criteria) this;
        }

        public Criteria andWorkshopLessThan(String value) {
            addCriterion("workshop <", value, "workshop");
            return (Criteria) this;
        }

        public Criteria andWorkshopLessThanOrEqualTo(String value) {
            addCriterion("workshop <=", value, "workshop");
            return (Criteria) this;
        }

        public Criteria andWorkshopLike(String value) {
            addCriterion("workshop like", value, "workshop");
            return (Criteria) this;
        }

        public Criteria andWorkshopNotLike(String value) {
            addCriterion("workshop not like", value, "workshop");
            return (Criteria) this;
        }

        public Criteria andWorkshopIn(List<String> values) {
            addCriterion("workshop in", values, "workshop");
            return (Criteria) this;
        }

        public Criteria andWorkshopNotIn(List<String> values) {
            addCriterion("workshop not in", values, "workshop");
            return (Criteria) this;
        }

        public Criteria andWorkshopBetween(String value1, String value2) {
            addCriterion("workshop between", value1, value2, "workshop");
            return (Criteria) this;
        }

        public Criteria andWorkshopNotBetween(String value1, String value2) {
            addCriterion("workshop not between", value1, value2, "workshop");
            return (Criteria) this;
        }

        public Criteria andUserIsNull() {
            addCriterion("User is null");
            return (Criteria) this;
        }

        public Criteria andUserIsNotNull() {
            addCriterion("User is not null");
            return (Criteria) this;
        }

        public Criteria andUserEqualTo(String value) {
            addCriterion("User =", value, "user");
            return (Criteria) this;
        }

        public Criteria andUserNotEqualTo(String value) {
            addCriterion("User <>", value, "user");
            return (Criteria) this;
        }

        public Criteria andUserGreaterThan(String value) {
            addCriterion("User >", value, "user");
            return (Criteria) this;
        }

        public Criteria andUserGreaterThanOrEqualTo(String value) {
            addCriterion("User >=", value, "user");
            return (Criteria) this;
        }

        public Criteria andUserLessThan(String value) {
            addCriterion("User <", value, "user");
            return (Criteria) this;
        }

        public Criteria andUserLessThanOrEqualTo(String value) {
            addCriterion("User <=", value, "user");
            return (Criteria) this;
        }

        public Criteria andUserLike(String value) {
            addCriterion("User like", value, "user");
            return (Criteria) this;
        }

        public Criteria andUserNotLike(String value) {
            addCriterion("User not like", value, "user");
            return (Criteria) this;
        }

        public Criteria andUserIn(List<String> values) {
            addCriterion("User in", values, "user");
            return (Criteria) this;
        }

        public Criteria andUserNotIn(List<String> values) {
            addCriterion("User not in", values, "user");
            return (Criteria) this;
        }

        public Criteria andUserBetween(String value1, String value2) {
            addCriterion("User between", value1, value2, "user");
            return (Criteria) this;
        }

        public Criteria andUserNotBetween(String value1, String value2) {
            addCriterion("User not between", value1, value2, "user");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}