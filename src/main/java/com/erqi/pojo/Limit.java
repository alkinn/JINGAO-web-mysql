package com.erqi.pojo;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

/**
 * @Author king
 * @Date 2020/6/5 13:43
 * @Version 1.0
 */

@Data
@ToString
public class Limit {
    protected Integer leftLimit;
    protected Integer limitSize;
    protected Date endDate;
    protected Integer page;
    protected Integer limit;
}
