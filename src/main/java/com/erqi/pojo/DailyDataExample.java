package com.erqi.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class DailyDataExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public DailyDataExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andDateIsNull() {
            addCriterion("date is null");
            return (Criteria) this;
        }

        public Criteria andDateIsNotNull() {
            addCriterion("date is not null");
            return (Criteria) this;
        }

        public Criteria andDateEqualTo(Date value) {
            addCriterionForJDBCDate("date =", value, "date");
            return (Criteria) this;
        }

        public Criteria andDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("date <>", value, "date");
            return (Criteria) this;
        }

        public Criteria andDateGreaterThan(Date value) {
            addCriterionForJDBCDate("date >", value, "date");
            return (Criteria) this;
        }

        public Criteria andDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("date >=", value, "date");
            return (Criteria) this;
        }

        public Criteria andDateLessThan(Date value) {
            addCriterionForJDBCDate("date <", value, "date");
            return (Criteria) this;
        }

        public Criteria andDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("date <=", value, "date");
            return (Criteria) this;
        }

        public Criteria andDateIn(List<Date> values) {
            addCriterionForJDBCDate("date in", values, "date");
            return (Criteria) this;
        }

        public Criteria andDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("date not in", values, "date");
            return (Criteria) this;
        }

        public Criteria andDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("date between", value1, value2, "date");
            return (Criteria) this;
        }

        public Criteria andDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("date not between", value1, value2, "date");
            return (Criteria) this;
        }

        public Criteria andKaifangIsNull() {
            addCriterion("kaifang is null");
            return (Criteria) this;
        }

        public Criteria andKaifangIsNotNull() {
            addCriterion("kaifang is not null");
            return (Criteria) this;
        }

        public Criteria andKaifangEqualTo(Integer value) {
            addCriterion("kaifang =", value, "kaifang");
            return (Criteria) this;
        }

        public Criteria andKaifangNotEqualTo(Integer value) {
            addCriterion("kaifang <>", value, "kaifang");
            return (Criteria) this;
        }

        public Criteria andKaifangGreaterThan(Integer value) {
            addCriterion("kaifang >", value, "kaifang");
            return (Criteria) this;
        }

        public Criteria andKaifangGreaterThanOrEqualTo(Integer value) {
            addCriterion("kaifang >=", value, "kaifang");
            return (Criteria) this;
        }

        public Criteria andKaifangLessThan(Integer value) {
            addCriterion("kaifang <", value, "kaifang");
            return (Criteria) this;
        }

        public Criteria andKaifangLessThanOrEqualTo(Integer value) {
            addCriterion("kaifang <=", value, "kaifang");
            return (Criteria) this;
        }

        public Criteria andKaifangIn(List<Integer> values) {
            addCriterion("kaifang in", values, "kaifang");
            return (Criteria) this;
        }

        public Criteria andKaifangNotIn(List<Integer> values) {
            addCriterion("kaifang not in", values, "kaifang");
            return (Criteria) this;
        }

        public Criteria andKaifangBetween(Integer value1, Integer value2) {
            addCriterion("kaifang between", value1, value2, "kaifang");
            return (Criteria) this;
        }

        public Criteria andKaifangNotBetween(Integer value1, Integer value2) {
            addCriterion("kaifang not between", value1, value2, "kaifang");
            return (Criteria) this;
        }

        public Criteria andYitiIsNull() {
            addCriterion("yiti is null");
            return (Criteria) this;
        }

        public Criteria andYitiIsNotNull() {
            addCriterion("yiti is not null");
            return (Criteria) this;
        }

        public Criteria andYitiEqualTo(Integer value) {
            addCriterion("yiti =", value, "yiti");
            return (Criteria) this;
        }

        public Criteria andYitiNotEqualTo(Integer value) {
            addCriterion("yiti <>", value, "yiti");
            return (Criteria) this;
        }

        public Criteria andYitiGreaterThan(Integer value) {
            addCriterion("yiti >", value, "yiti");
            return (Criteria) this;
        }

        public Criteria andYitiGreaterThanOrEqualTo(Integer value) {
            addCriterion("yiti >=", value, "yiti");
            return (Criteria) this;
        }

        public Criteria andYitiLessThan(Integer value) {
            addCriterion("yiti <", value, "yiti");
            return (Criteria) this;
        }

        public Criteria andYitiLessThanOrEqualTo(Integer value) {
            addCriterion("yiti <=", value, "yiti");
            return (Criteria) this;
        }

        public Criteria andYitiIn(List<Integer> values) {
            addCriterion("yiti in", values, "yiti");
            return (Criteria) this;
        }

        public Criteria andYitiNotIn(List<Integer> values) {
            addCriterion("yiti not in", values, "yiti");
            return (Criteria) this;
        }

        public Criteria andYitiBetween(Integer value1, Integer value2) {
            addCriterion("yiti between", value1, value2, "yiti");
            return (Criteria) this;
        }

        public Criteria andYitiNotBetween(Integer value1, Integer value2) {
            addCriterion("yiti not between", value1, value2, "yiti");
            return (Criteria) this;
        }

        public Criteria andAllsIsNull() {
            addCriterion("alls is null");
            return (Criteria) this;
        }

        public Criteria andAllsIsNotNull() {
            addCriterion("alls is not null");
            return (Criteria) this;
        }

        public Criteria andAllsEqualTo(Integer value) {
            addCriterion("alls =", value, "alls");
            return (Criteria) this;
        }

        public Criteria andAllsNotEqualTo(Integer value) {
            addCriterion("alls <>", value, "alls");
            return (Criteria) this;
        }

        public Criteria andAllsGreaterThan(Integer value) {
            addCriterion("alls >", value, "alls");
            return (Criteria) this;
        }

        public Criteria andAllsGreaterThanOrEqualTo(Integer value) {
            addCriterion("alls >=", value, "alls");
            return (Criteria) this;
        }

        public Criteria andAllsLessThan(Integer value) {
            addCriterion("alls <", value, "alls");
            return (Criteria) this;
        }

        public Criteria andAllsLessThanOrEqualTo(Integer value) {
            addCriterion("alls <=", value, "alls");
            return (Criteria) this;
        }

        public Criteria andAllsIn(List<Integer> values) {
            addCriterion("alls in", values, "alls");
            return (Criteria) this;
        }

        public Criteria andAllsNotIn(List<Integer> values) {
            addCriterion("alls not in", values, "alls");
            return (Criteria) this;
        }

        public Criteria andAllsBetween(Integer value1, Integer value2) {
            addCriterion("alls between", value1, value2, "alls");
            return (Criteria) this;
        }

        public Criteria andAllsNotBetween(Integer value1, Integer value2) {
            addCriterion("alls not between", value1, value2, "alls");
            return (Criteria) this;
        }

        public Criteria and1kIsNull() {
            addCriterion("1k is null");
            return (Criteria) this;
        }

        public Criteria and1kIsNotNull() {
            addCriterion("1k is not null");
            return (Criteria) this;
        }

        public Criteria and1kEqualTo(Integer value) {
            addCriterion("1k =", value, "1k");
            return (Criteria) this;
        }

        public Criteria and1kNotEqualTo(Integer value) {
            addCriterion("1k <>", value, "1k");
            return (Criteria) this;
        }

        public Criteria and1kGreaterThan(Integer value) {
            addCriterion("1k >", value, "1k");
            return (Criteria) this;
        }

        public Criteria and1kGreaterThanOrEqualTo(Integer value) {
            addCriterion("1k >=", value, "1k");
            return (Criteria) this;
        }

        public Criteria and1kLessThan(Integer value) {
            addCriterion("1k <", value, "1k");
            return (Criteria) this;
        }

        public Criteria and1kLessThanOrEqualTo(Integer value) {
            addCriterion("1k <=", value, "1k");
            return (Criteria) this;
        }

        public Criteria and1kIn(List<Integer> values) {
            addCriterion("1k in", values, "1k");
            return (Criteria) this;
        }

        public Criteria and1kNotIn(List<Integer> values) {
            addCriterion("1k not in", values, "1k");
            return (Criteria) this;
        }

        public Criteria and1kBetween(Integer value1, Integer value2) {
            addCriterion("1k between", value1, value2, "1k");
            return (Criteria) this;
        }

        public Criteria and1kNotBetween(Integer value1, Integer value2) {
            addCriterion("1k not between", value1, value2, "1k");
            return (Criteria) this;
        }

        public Criteria and1tIsNull() {
            addCriterion("1t is null");
            return (Criteria) this;
        }

        public Criteria and1tIsNotNull() {
            addCriterion("1t is not null");
            return (Criteria) this;
        }

        public Criteria and1tEqualTo(Integer value) {
            addCriterion("1t =", value, "1t");
            return (Criteria) this;
        }

        public Criteria and1tNotEqualTo(Integer value) {
            addCriterion("1t <>", value, "1t");
            return (Criteria) this;
        }

        public Criteria and1tGreaterThan(Integer value) {
            addCriterion("1t >", value, "1t");
            return (Criteria) this;
        }

        public Criteria and1tGreaterThanOrEqualTo(Integer value) {
            addCriterion("1t >=", value, "1t");
            return (Criteria) this;
        }

        public Criteria and1tLessThan(Integer value) {
            addCriterion("1t <", value, "1t");
            return (Criteria) this;
        }

        public Criteria and1tLessThanOrEqualTo(Integer value) {
            addCriterion("1t <=", value, "1t");
            return (Criteria) this;
        }

        public Criteria and1tIn(List<Integer> values) {
            addCriterion("1t in", values, "1t");
            return (Criteria) this;
        }

        public Criteria and1tNotIn(List<Integer> values) {
            addCriterion("1t not in", values, "1t");
            return (Criteria) this;
        }

        public Criteria and1tBetween(Integer value1, Integer value2) {
            addCriterion("1t between", value1, value2, "1t");
            return (Criteria) this;
        }

        public Criteria and1tNotBetween(Integer value1, Integer value2) {
            addCriterion("1t not between", value1, value2, "1t");
            return (Criteria) this;
        }

        public Criteria and2kIsNull() {
            addCriterion("2k is null");
            return (Criteria) this;
        }

        public Criteria and2kIsNotNull() {
            addCriterion("2k is not null");
            return (Criteria) this;
        }

        public Criteria and2kEqualTo(Integer value) {
            addCriterion("2k =", value, "2k");
            return (Criteria) this;
        }

        public Criteria and2kNotEqualTo(Integer value) {
            addCriterion("2k <>", value, "2k");
            return (Criteria) this;
        }

        public Criteria and2kGreaterThan(Integer value) {
            addCriterion("2k >", value, "2k");
            return (Criteria) this;
        }

        public Criteria and2kGreaterThanOrEqualTo(Integer value) {
            addCriterion("2k >=", value, "2k");
            return (Criteria) this;
        }

        public Criteria and2kLessThan(Integer value) {
            addCriterion("2k <", value, "2k");
            return (Criteria) this;
        }

        public Criteria and2kLessThanOrEqualTo(Integer value) {
            addCriterion("2k <=", value, "2k");
            return (Criteria) this;
        }

        public Criteria and2kIn(List<Integer> values) {
            addCriterion("2k in", values, "2k");
            return (Criteria) this;
        }

        public Criteria and2kNotIn(List<Integer> values) {
            addCriterion("2k not in", values, "2k");
            return (Criteria) this;
        }

        public Criteria and2kBetween(Integer value1, Integer value2) {
            addCriterion("2k between", value1, value2, "2k");
            return (Criteria) this;
        }

        public Criteria and2kNotBetween(Integer value1, Integer value2) {
            addCriterion("2k not between", value1, value2, "2k");
            return (Criteria) this;
        }

        public Criteria and2tIsNull() {
            addCriterion("2t is null");
            return (Criteria) this;
        }

        public Criteria and2tIsNotNull() {
            addCriterion("2t is not null");
            return (Criteria) this;
        }

        public Criteria and2tEqualTo(Integer value) {
            addCriterion("2t =", value, "2t");
            return (Criteria) this;
        }

        public Criteria and2tNotEqualTo(Integer value) {
            addCriterion("2t <>", value, "2t");
            return (Criteria) this;
        }

        public Criteria and2tGreaterThan(Integer value) {
            addCriterion("2t >", value, "2t");
            return (Criteria) this;
        }

        public Criteria and2tGreaterThanOrEqualTo(Integer value) {
            addCriterion("2t >=", value, "2t");
            return (Criteria) this;
        }

        public Criteria and2tLessThan(Integer value) {
            addCriterion("2t <", value, "2t");
            return (Criteria) this;
        }

        public Criteria and2tLessThanOrEqualTo(Integer value) {
            addCriterion("2t <=", value, "2t");
            return (Criteria) this;
        }

        public Criteria and2tIn(List<Integer> values) {
            addCriterion("2t in", values, "2t");
            return (Criteria) this;
        }

        public Criteria and2tNotIn(List<Integer> values) {
            addCriterion("2t not in", values, "2t");
            return (Criteria) this;
        }

        public Criteria and2tBetween(Integer value1, Integer value2) {
            addCriterion("2t between", value1, value2, "2t");
            return (Criteria) this;
        }

        public Criteria and2tNotBetween(Integer value1, Integer value2) {
            addCriterion("2t not between", value1, value2, "2t");
            return (Criteria) this;
        }

        public Criteria and3kIsNull() {
            addCriterion("3k is null");
            return (Criteria) this;
        }

        public Criteria and3kIsNotNull() {
            addCriterion("3k is not null");
            return (Criteria) this;
        }

        public Criteria and3kEqualTo(Integer value) {
            addCriterion("3k =", value, "3k");
            return (Criteria) this;
        }

        public Criteria and3kNotEqualTo(Integer value) {
            addCriterion("3k <>", value, "3k");
            return (Criteria) this;
        }

        public Criteria and3kGreaterThan(Integer value) {
            addCriterion("3k >", value, "3k");
            return (Criteria) this;
        }

        public Criteria and3kGreaterThanOrEqualTo(Integer value) {
            addCriterion("3k >=", value, "3k");
            return (Criteria) this;
        }

        public Criteria and3kLessThan(Integer value) {
            addCriterion("3k <", value, "3k");
            return (Criteria) this;
        }

        public Criteria and3kLessThanOrEqualTo(Integer value) {
            addCriterion("3k <=", value, "3k");
            return (Criteria) this;
        }

        public Criteria and3kIn(List<Integer> values) {
            addCriterion("3k in", values, "3k");
            return (Criteria) this;
        }

        public Criteria and3kNotIn(List<Integer> values) {
            addCriterion("3k not in", values, "3k");
            return (Criteria) this;
        }

        public Criteria and3kBetween(Integer value1, Integer value2) {
            addCriterion("3k between", value1, value2, "3k");
            return (Criteria) this;
        }

        public Criteria and3kNotBetween(Integer value1, Integer value2) {
            addCriterion("3k not between", value1, value2, "3k");
            return (Criteria) this;
        }

        public Criteria and3tIsNull() {
            addCriterion("3t is null");
            return (Criteria) this;
        }

        public Criteria and3tIsNotNull() {
            addCriterion("3t is not null");
            return (Criteria) this;
        }

        public Criteria and3tEqualTo(Integer value) {
            addCriterion("3t =", value, "3t");
            return (Criteria) this;
        }

        public Criteria and3tNotEqualTo(Integer value) {
            addCriterion("3t <>", value, "3t");
            return (Criteria) this;
        }

        public Criteria and3tGreaterThan(Integer value) {
            addCriterion("3t >", value, "3t");
            return (Criteria) this;
        }

        public Criteria and3tGreaterThanOrEqualTo(Integer value) {
            addCriterion("3t >=", value, "3t");
            return (Criteria) this;
        }

        public Criteria and3tLessThan(Integer value) {
            addCriterion("3t <", value, "3t");
            return (Criteria) this;
        }

        public Criteria and3tLessThanOrEqualTo(Integer value) {
            addCriterion("3t <=", value, "3t");
            return (Criteria) this;
        }

        public Criteria and3tIn(List<Integer> values) {
            addCriterion("3t in", values, "3t");
            return (Criteria) this;
        }

        public Criteria and3tNotIn(List<Integer> values) {
            addCriterion("3t not in", values, "3t");
            return (Criteria) this;
        }

        public Criteria and3tBetween(Integer value1, Integer value2) {
            addCriterion("3t between", value1, value2, "3t");
            return (Criteria) this;
        }

        public Criteria and3tNotBetween(Integer value1, Integer value2) {
            addCriterion("3t not between", value1, value2, "3t");
            return (Criteria) this;
        }

        public Criteria and4kIsNull() {
            addCriterion("4k is null");
            return (Criteria) this;
        }

        public Criteria and4kIsNotNull() {
            addCriterion("4k is not null");
            return (Criteria) this;
        }

        public Criteria and4kEqualTo(Integer value) {
            addCriterion("4k =", value, "4k");
            return (Criteria) this;
        }

        public Criteria and4kNotEqualTo(Integer value) {
            addCriterion("4k <>", value, "4k");
            return (Criteria) this;
        }

        public Criteria and4kGreaterThan(Integer value) {
            addCriterion("4k >", value, "4k");
            return (Criteria) this;
        }

        public Criteria and4kGreaterThanOrEqualTo(Integer value) {
            addCriterion("4k >=", value, "4k");
            return (Criteria) this;
        }

        public Criteria and4kLessThan(Integer value) {
            addCriterion("4k <", value, "4k");
            return (Criteria) this;
        }

        public Criteria and4kLessThanOrEqualTo(Integer value) {
            addCriterion("4k <=", value, "4k");
            return (Criteria) this;
        }

        public Criteria and4kIn(List<Integer> values) {
            addCriterion("4k in", values, "4k");
            return (Criteria) this;
        }

        public Criteria and4kNotIn(List<Integer> values) {
            addCriterion("4k not in", values, "4k");
            return (Criteria) this;
        }

        public Criteria and4kBetween(Integer value1, Integer value2) {
            addCriterion("4k between", value1, value2, "4k");
            return (Criteria) this;
        }

        public Criteria and4kNotBetween(Integer value1, Integer value2) {
            addCriterion("4k not between", value1, value2, "4k");
            return (Criteria) this;
        }

        public Criteria and4tIsNull() {
            addCriterion("4t is null");
            return (Criteria) this;
        }

        public Criteria and4tIsNotNull() {
            addCriterion("4t is not null");
            return (Criteria) this;
        }

        public Criteria and4tEqualTo(Integer value) {
            addCriterion("4t =", value, "4t");
            return (Criteria) this;
        }

        public Criteria and4tNotEqualTo(Integer value) {
            addCriterion("4t <>", value, "4t");
            return (Criteria) this;
        }

        public Criteria and4tGreaterThan(Integer value) {
            addCriterion("4t >", value, "4t");
            return (Criteria) this;
        }

        public Criteria and4tGreaterThanOrEqualTo(Integer value) {
            addCriterion("4t >=", value, "4t");
            return (Criteria) this;
        }

        public Criteria and4tLessThan(Integer value) {
            addCriterion("4t <", value, "4t");
            return (Criteria) this;
        }

        public Criteria and4tLessThanOrEqualTo(Integer value) {
            addCriterion("4t <=", value, "4t");
            return (Criteria) this;
        }

        public Criteria and4tIn(List<Integer> values) {
            addCriterion("4t in", values, "4t");
            return (Criteria) this;
        }

        public Criteria and4tNotIn(List<Integer> values) {
            addCriterion("4t not in", values, "4t");
            return (Criteria) this;
        }

        public Criteria and4tBetween(Integer value1, Integer value2) {
            addCriterion("4t between", value1, value2, "4t");
            return (Criteria) this;
        }

        public Criteria and4tNotBetween(Integer value1, Integer value2) {
            addCriterion("4t not between", value1, value2, "4t");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}