package com.erqi.dao;

import com.erqi.pojo.Limit;
import com.erqi.pojo.Qrandplc;
import com.erqi.pojo.QrandplcExample;

import java.util.List;

/**
 * @Author king
 * @Date 2020/6/5 9:51
 * @Version 1.0
 */
public interface plcDao {

    Qrandplc QueryPlcById(Integer id);
    Integer selectByQrandplc(Qrandplc qrandplc);
    int insertSelective(Qrandplc qrandplc);
    List<Qrandplc> selectByExample( Qrandplc qrandplc, Limit limit);
    Long selcecount(Qrandplc qrandplc, Limit limit);
    int AddQrandplc(Qrandplc qrandplc);
    int DelList(List<Integer> DList);
    int DeleteList(List<Integer> DList);
    List<Qrandplc> queryAll();
    long[] getKai();
    int modifyQeandplcData(Qrandplc qrandplc);

}
