package com.erqi.dao.impl;

import com.erqi.dao.userDao;
import com.erqi.mapper.PlcuserMapper;
import com.erqi.pojo.Plcuser;
import com.erqi.pojo.PlcuserExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author king
 * @Date 2020/7/24 15:43
 * @Version 1.0
 */
@Service
public class userDaoImpl implements userDao {

    @Autowired
    private PlcuserMapper userMapper;

    /**
     * 根据条件用户条件获取用户
     * @param user
     * @return
     */
    @Override
    public List<Plcuser> GainUser(Plcuser user) {

        PlcuserExample example = new PlcuserExample();
        PlcuserExample.Criteria criteria = example.createCriteria();
        if (user!=null){
            if (user.getName()!=null && !"".equals(user.getName())){
                criteria.andNameEqualTo(user.getName());
            }
            if (user.getUsername()!=null && !"".equals(user.getUsername())){
                criteria.andUsernameEqualTo(user.getUsername());
            }
            if (user.getPhone()!=null&& !"".equals(user.getPhone())){
                criteria.andPhoneEqualTo(user.getPhone());
            }
            if (user.getRoles()!=null && !"".equals(user.getRoles())){
                criteria.andRolesEqualTo(user.getRoles());
            }
            if (user.getId()!=null && user.getId()>=0){
                criteria.andIdEqualTo(user.getId());
            }
            return userMapper.selectByExample(example);
        }

        return userMapper.selectByExample(example);

    }

    /**
     *    保存一个用户
     * @param user
     * @return
     */
    @Override
    public int saveUser(Plcuser user){
        if (user!=null){
           return userMapper.insertSelective(user);
        }else{
            return 0;
        }
    }

    /**
     * 删除用户 （不可逆直接删除）
     * @param user
     * @return
     */
    @Override
    public int delUser(Plcuser user) {
        if (user!=null){
            return userMapper.deleteByPrimaryKey(user.getId()); //删除成功返回1
        }else{
            return 0;//删除失败返回0
        }
    }

    /**
     * 修改用户
     * @param user
     * @return
     */
    @Override
    public int modifyUser(Plcuser user) {

        if (user!=null){
            if (user.getId()==null || "".equals(user.getId())) {
                return 0;//失败返回0
            }else {
                //判断修改的用户是员工还是主管
                if ("1".equals(user.getRoles())){
                    user.setRoles("leader");
                }else if("0".equals(user.getRoles())){
                    user.setRoles("staff");
                }
                return userMapper.updateByPrimaryKeySelective(user); //成功返回 1
            }
        }else {
            return 0;//失败返回0
        }
    }

    @Override
    public int modifyPassword(String newPassword, String oldPassword,Integer id) {
        if (("".equals(newPassword)&&"".equals(oldPassword))||(newPassword==null&&oldPassword==null)){
            return 0;//新密码旧密码为空返回0
        }else{

            Plcuser plcuser = userMapper.selectByPrimaryKey(id);
            if (plcuser!=null){
                if (plcuser.getPassword().equals(oldPassword)){
                    plcuser.setPassword(newPassword);
                   return userMapper.updateByPrimaryKey(plcuser);
                }else{
                    return 2;//
                }
            }else{
                return 0;
            }
        }
    }
}
