package com.erqi.dao.impl;

import com.erqi.dao.yalUIDao;
import com.erqi.mapper.QrandplcMapper;
import com.erqi.pojo.Limit;
import com.erqi.pojo.Qrandplc;
import com.erqi.pojo.QrandplcExample;
import com.erqi.uitl.dates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author king
 * @Date 2020/6/8 16:27
 * @Version 1.0
 */

@Service
public class yalUIImpl implements yalUIDao {
    @Autowired
    QrandplcMapper qrandplcMapper;


    /**
     * 分页查询
     * @param qrandplc
     * @param limit
     * @return
     */
    @Override
    public List<Qrandplc> selectByExample(Qrandplc qrandplc, Limit limit) {
        QrandplcExample example =new QrandplcExample();

        //limit page,limit
        example.setPage(limit.getLimit()*(limit.getPage()-1));
        example.setLimit(limit.getLimit());

        QrandplcExample.Criteria criteria = example.createCriteria();

        //判断 单晶号如果存在只进行单晶号查询
        if (qrandplc.getQrcode()!=null && qrandplc.getQrcode().length()>1){
            QrandplcExample examples =new QrandplcExample();
            examples.setPage(limit.getLimit()*(limit.getPage()-1));
            examples.setLimit(limit.getLimit());
            examples.createCriteria().andQrcodeEqualTo(qrandplc.getQrcode());
            return qrandplcMapper.NEWselectByExample(examples);
        }
        //判断 用户角色是否非空
        if (qrandplc.getUser()!=null && qrandplc.getUser().length()>1){

            QrandplcExample examples =new QrandplcExample();
            examples.setPage(limit.getLimit()*(limit.getPage()-1));
            examples.setLimit(limit.getLimit());
            examples.createCriteria()
                    .andDateBetween(qrandplc.getDate(),limit.getEndDate())
                    .andUserEqualTo(qrandplc.getUser())
                    .andTypeEqualTo(qrandplc.getType())
                    .andState1EqualTo(qrandplc.getState1());
            return qrandplcMapper.NEWselectByExample(examples);
        }

        criteria.andDateBetween(qrandplc.getDate(),limit.getEndDate());
        if (qrandplc.getType()!=null){
            criteria.andTypeEqualTo(qrandplc.getType());
        }
        if (qrandplc.getState1()!=null){

            criteria.andState1EqualTo(qrandplc.getState1());
        }
        if (qrandplc.getState1()==null){
            qrandplc.setState1("入库成功");
            criteria.andState1EqualTo(qrandplc.getState1());
        }
        if (qrandplc.getWorkshop()!=null){
            criteria.andWorkshopEqualTo(qrandplc.getWorkshop());
        }
        if (qrandplc.getNumMachine()!=null &&qrandplc.getNumMachine().length()>0){
            criteria.andNumMachineEqualTo(qrandplc.getNumMachine());
        }

        return qrandplcMapper.NEWselectByExample(example);
    }


    /**
     * get30今日共计
     * @return
     */
    public long getAllOfToday(){
        QrandplcExample example =new QrandplcExample();
        QrandplcExample.Criteria criteria = example.createCriteria();
        criteria.andDateBetween(dates.get0(),dates.get24());
        criteria.andState1EqualTo("入库成功");
        return qrandplcMapper.countByExample(example);
    }

    /**
     * get30s 今日重复
     * @return
     */
    public long  getOfTodayRepeat(){
        QrandplcExample example =new QrandplcExample();
        QrandplcExample.Criteria criteria = example.createCriteria();
        criteria.andDateBetween(dates.get0(),dates.get24());
        criteria.andState1EqualTo("重复");
        return qrandplcMapper.countByExample(example);
    }

    /**
     * get30s 今日开方机
     * @return
     */
    public long  getOfTodayKaifang(){
        QrandplcExample example =new QrandplcExample();
        QrandplcExample.Criteria criteria = example.createCriteria();
        criteria.andDateBetween(dates.get0(),dates.get24());
        criteria.andState1EqualTo("入库成功");
        criteria.andTypeEqualTo("开方机");
        return qrandplcMapper.countByExample(example);
    }
    /**
     * get30s 今日一体机
     * @return
     */
    public long  getOfTodayYiti(){
        QrandplcExample example =new QrandplcExample();
        QrandplcExample.Criteria criteria = example.createCriteria();
        criteria.andDateBetween(dates.get0(),dates.get24());
        criteria.andState1EqualTo("入库成功");
        criteria.andTypeEqualTo("一体机");
        return qrandplcMapper.countByExample(example);
    }


    /**
     * 删除数据并不是真正意义的删除而是吧状态吗修改为删除
     * @param DList
     * @return
     */
    public int DelList(List<Integer> DList){
        Qrandplc qrandplc=new Qrandplc();
        qrandplc.setState1("删除");
        QrandplcExample example =new QrandplcExample();
        QrandplcExample.Criteria criteria = example.createCriteria();
        criteria.andIdIn(DList);
        return qrandplcMapper.updateByExampleSelective(qrandplc,example);

    }
}
