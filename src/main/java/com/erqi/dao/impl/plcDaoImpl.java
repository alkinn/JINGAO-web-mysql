package com.erqi.dao.impl;

import com.erqi.dao.plcDao;
import com.erqi.mapper.QrandplcMapper;
import com.erqi.pojo.Limit;
import com.erqi.pojo.Plcuser;
import com.erqi.pojo.Qrandplc;
import com.erqi.pojo.QrandplcExample;
import com.erqi.uitl.dates;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.crypto.Data;
import java.util.Date;
import java.util.List;

/**
 * @Author king
 * @Date 2020/6/5 9:52
 * @Version 1.0
 */
@Service
public class plcDaoImpl implements plcDao {

    Logger log = Logger.getLogger(plcDaoImpl.class);
    @Autowired
    QrandplcMapper qrandplcMapper;


    /**
     * 根据id查询
     * @param id 主键id
     * @return  试图
     */
    @Override
    public Qrandplc QueryPlcById(Integer id) {
        if (id>0){
            return qrandplcMapper.selectByPrimaryKey(id);
        }else{
            return null;
        }
    }

    /**
     * 自定mapper 根据qrplc 中的 code 和 type 进行查询有无
     * @param qrandplc pojo
     * @return int
     */
    @Override
    public Integer selectByQrandplc(Qrandplc qrandplc) {
        return qrandplcMapper.selectByQrandplc(qrandplc);
    }

    /**
     * 保存 方法 根据实体类进行持久化操作
     * @param qrandplc pojo
     * @return  int
     */
    @Override
    public int insertSelective(Qrandplc qrandplc) {
        return qrandplcMapper.insertSelective(qrandplc);
    }

    /**
     * 时间段查询 带分页 业务实现
     * @param qrandplc 实体类
     * @param limit limit 属性
     * @return list
     */
    @Override
    public List<Qrandplc> selectByExample(Qrandplc qrandplc, Limit limit) {
        QrandplcExample example =new QrandplcExample();
        example.setLimitSize(limit.getLimitSize());
        example.setLeftLimit(limit.getLeftLimit()*limit.getLimitSize());
        QrandplcExample.Criteria criteria = example.createCriteria();
        if (qrandplc.getQrcode()!=null && qrandplc.getQrcode().length()>1){
            QrandplcExample examples =new QrandplcExample();
            examples.setLimitSize(limit.getLimitSize());
            examples.setLeftLimit(limit.getLeftLimit()*limit.getLimitSize());
            examples.createCriteria().andQrcodeEqualTo(qrandplc.getQrcode());
            return qrandplcMapper.selectByExample(examples);
        }

        criteria.andDateBetween(qrandplc.getDate(),limit.getEndDate());
        if (qrandplc.getType()!=null){
            criteria.andTypeEqualTo(qrandplc.getType());
        }
        if (qrandplc.getState1()!=null){
            criteria.andState1EqualTo(qrandplc.getState1());
        }
        if (qrandplc.getWorkshop()!=null){
            criteria.andWorkshopEqualTo(qrandplc.getWorkshop());
        }
        if (qrandplc.getNumMachine()!=null &&qrandplc.getNumMachine().length()>0){
            criteria.andNumMachineEqualTo(qrandplc.getNumMachine());
        }

        return qrandplcMapper.selectByExample(example);
    }

    /**
     * 获取 当前条件总行数
     * @param qrandplc 实体类
     * @param limit 开始时间结束结束
     * @return long
     */
    public Long selcecount(Qrandplc qrandplc, Limit limit){
        System.out.println("------------"+qrandplc);
        QrandplcExample example =new QrandplcExample();
        QrandplcExample.Criteria criteria = example.createCriteria();
        if (qrandplc.getQrcode()!=null && qrandplc.getQrcode().length()>1){
            QrandplcExample examples =new QrandplcExample();
            examples.createCriteria().andQrcodeEqualTo(qrandplc.getQrcode());
            return qrandplcMapper.countByExample(examples);
        }
        criteria.andDateBetween(qrandplc.getDate(),limit.getEndDate());
        if (qrandplc.getType()!=null){
            criteria.andTypeEqualTo(qrandplc.getType());
        }
        if (qrandplc.getState1()!=null){
            criteria.andState1EqualTo(qrandplc.getState1());
        }
        if (qrandplc.getWorkshop()!=null){
            criteria.andWorkshopEqualTo(qrandplc.getWorkshop());
        }
        if (qrandplc.getNumMachine()!=null  &&qrandplc.getNumMachine().length()>0){
            criteria.andNumMachineEqualTo(qrandplc.getNumMachine());
        }
        if (qrandplc.getUser()!=null&&qrandplc.getUser().length()>0){
            criteria.andUserEqualTo(qrandplc.getUser());
        }

        return qrandplcMapper.countByExample(example);


    }


    /**
     * 添加一条数据
     * @param qrandplc 实体类
     * @return int
     */
    public int AddQrandplc(Qrandplc qrandplc){
        qrandplc.setDate(new Date());
        String numMachine = qrandplc.getNumMachine();
        if (numMachine!=null) {
            //拿到当前登录用对象的user
            //获取到当前的登录的对象
            Plcuser plcuser = (Plcuser) SecurityUtils.getSubject().getPrincipal();
            qrandplc.setType(qrandplc.getType());
            if (qrandplc.getUser()==null){
                qrandplc.setUser(plcuser.getName());
            }
            qrandplc.setState1("入库成功");
            qrandplc.setWorkshop("2");
            qrandplc.setMonitor(qrandplcMapper.selectMonitor());
            log.info("添加一条用户为：{}"+plcuser.getName());
            return qrandplcMapper.insertSelective(qrandplc);
        }else{
            return 0;
        }

    }



    public int DeleteList(List<Integer> DList){
        QrandplcExample example =new QrandplcExample();
        QrandplcExample.Criteria criteria = example.createCriteria();
        criteria.andIdIn(DList);
        return qrandplcMapper.deleteByExample(example);
    }

    /**
     * 删除这里的删除不是真正意义上的删除而是吧 状态改为删除状态
     * @param
     * @return
     */
    public int DelList(List<Integer> DList){
        Qrandplc qrandplc=new Qrandplc();
        qrandplc.setState1("删除");
        QrandplcExample example =new QrandplcExample();
        QrandplcExample.Criteria criteria = example.createCriteria();
        criteria.andIdIn(DList);
        return qrandplcMapper.updateByExampleSelective(qrandplc,example);

    }

    /**
     * 查询所有
     * @return kong
     */
    public List<Qrandplc> queryAll(){
        return  qrandplcMapper.selectAll();
    }



    public long[] getKai(){
        long[] longs = new long[3];
        QrandplcExample example =new QrandplcExample();
        for (int i = 0; i <2 ; i++) {
            switch (i){
                case 0:
                    example.createCriteria().andDateBetween(dates.getPastDate(1,0),dates.getPastDate(1,24)).andTypeEqualTo("开方机").andState1EqualTo("入库成功");
                    longs[0]=qrandplcMapper.countByExample(example);
                    break;
                case 1:
                    example.clear();
                    example.createCriteria().andDateBetween(dates.getPastDate(1,0),dates.getPastDate(1,24)).andTypeEqualTo("一体机").andState1EqualTo("入库成功");
                    longs[1]=qrandplcMapper.countByExample(example);
                    break;
            }
        }
        longs[2]=longs[0]+longs[1];
        return longs;
    }

    /**
     * 获取当天的 开方机和一体机的所有线体的分别数
     * @return
     * start
     */
    public long getToday1K(){
        QrandplcExample example =new QrandplcExample();
        example.createCriteria().andDateBetween(dates.getPastDate(1,0),dates.getPastDate(1,24))
                .andNumRobotEqualTo("1")
                .andTypeEqualTo("开方机").andState1EqualTo("入库成功");
        return qrandplcMapper.countByExample(example);
    }
    public long getToday1T(){
        QrandplcExample example =new QrandplcExample();
        example.createCriteria().andDateBetween(dates.getPastDate(1,0),dates.getPastDate(1,24))
                .andNumRobotEqualTo("1")
                .andTypeEqualTo("一体机").andState1EqualTo("入库成功");
        return qrandplcMapper.countByExample(example);
    }
    public long getToday2K(){
        QrandplcExample example =new QrandplcExample();
        example.createCriteria().andDateBetween(dates.getPastDate(1,0),dates.getPastDate(1,24))
                .andNumRobotEqualTo("2")
                .andTypeEqualTo("开方机").andState1EqualTo("入库成功");
        return qrandplcMapper.countByExample(example);
    }
    public long getToday2T(){
        QrandplcExample example =new QrandplcExample();
        example.createCriteria().andDateBetween(dates.getPastDate(1,0),dates.getPastDate(1,24))
                .andNumRobotEqualTo("2")
                .andTypeEqualTo("一体机").andState1EqualTo("入库成功");
        return qrandplcMapper.countByExample(example);
    }
    public long getToday3K(){
        QrandplcExample example =new QrandplcExample();
        example.createCriteria().andDateBetween(dates.getPastDate(1,0),dates.getPastDate(1,24))
                .andNumRobotEqualTo("3")
                .andTypeEqualTo("开方机").andState1EqualTo("入库成功");
        return qrandplcMapper.countByExample(example);
    }
    public long getToday3T(){
        QrandplcExample example =new QrandplcExample();
        example.createCriteria().andDateBetween(dates.getPastDate(1,0),dates.getPastDate(1,24))
                .andNumRobotEqualTo("3")
                .andTypeEqualTo("一体机").andState1EqualTo("入库成功");
        return qrandplcMapper.countByExample(example);
    }
    public long getToday4K(){
        QrandplcExample example =new QrandplcExample();
        example.createCriteria().andDateBetween(dates.getPastDate(1,0),dates.getPastDate(1,24))
                .andNumRobotEqualTo("4")
                .andTypeEqualTo("开方机").andState1EqualTo("入库成功");
        return qrandplcMapper.countByExample(example);
    }
    public long getToday4T(){
        QrandplcExample example =new QrandplcExample();
        example.createCriteria().andDateBetween(dates.getPastDate(1,0),dates.getPastDate(1,24))
                .andNumRobotEqualTo("4")
                .andTypeEqualTo("一体机").andState1EqualTo("入库成功");
        return qrandplcMapper.countByExample(example);
    }
    /**
     * 获取当天的 开方机和一体机的所有线体的分别数
     * @return
     * end
     */





    @Override
    public int modifyQeandplcData(Qrandplc qrandplc) {
        if (qrandplc!=null){
           return qrandplcMapper.updateByPrimaryKeySelective(qrandplc);//成功返回1
        }else{
            return 0;//失败返回0
        }
    }
}
