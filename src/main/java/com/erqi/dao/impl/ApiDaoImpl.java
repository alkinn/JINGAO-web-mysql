package com.erqi.dao.impl;

import com.erqi.dao.ApiDao;
import com.erqi.mapper.DailyDataMapper;
import com.erqi.mapper.QrandplcMapper;
import com.erqi.pojo.DailyData;
import com.erqi.pojo.DailyDataExample;
import com.erqi.pojo.Qrandplc;
import com.erqi.pojo.QrandplcExample;
import com.erqi.uitl.dates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @Author king
 * @Date 2020/6/28 16:31
 * @Version 1.0
 *
 *
 * 这里是曲线图的dao 单独分出来了
 *
 */
@Service
public class ApiDaoImpl implements ApiDao {

    @Autowired
    DailyDataMapper dataMapper;

    @Autowired
    QrandplcMapper qrandplcMapper;


    /**
     * 从数据库获取七天的数据。
     * 
     * @return
     */
    public List<DailyData> GetAllData(){
        DailyDataExample example = new DailyDataExample();
        DailyDataExample.Criteria criteria= example.createCriteria();
        criteria.andDateBetween(dates.getPastDate(7,0),dates.get0());
        return dataMapper.selectByExample(example);
    }


    /**
     * 获取当日开放或者一体机根据code查询数量小于2的plcData
     * @param statsData
     * @param endData
     * @return
     */
    public List<Qrandplc> getNo1(Date statsData,Date endData){
        QrandplcExample example = new QrandplcExample();
        QrandplcExample.Criteria criteria= example.createCriteria();
        criteria.andDateBetween(statsData,endData);

        List<Qrandplc> qrandplcs = qrandplcMapper.selectByExample(example);
        List<Qrandplc> plcs = new ArrayList<Qrandplc>();

        for (Qrandplc qrandplc : qrandplcs) {
            example.clear();//查询出今日所有数量之后进行循环 只要是当日根据qrcode 查询出来的数量小于2的全部塞入到自定义集合中
            example.createCriteria().andQrcodeEqualTo(qrandplc.getQrcode());
            long l1 = qrandplcMapper.countByExample(example);
            if (l1<2){
                plcs.add(qrandplc);
            }
        }



        return plcs;

    }


    /**
     * 甲方需求 start 对公api接口 根据单晶号查询
     * @param Qrcode
     * @return
     */
    @Override
    public List<Qrandplc> QueryDataByQRcode(String Qrcode) {
        QrandplcExample example = new QrandplcExample();
        QrandplcExample.Criteria criteria= example.createCriteria();
        criteria.andQrcodeEqualTo(Qrcode);
        return qrandplcMapper.selectExample(example);
    }

    /**
     * 甲方需求 根据两个时间段进行查询
     * @param startDate
     * @param endDate
     * @return
     */
    public  List<Qrandplc> QueryDataByDate(Date startDate,Date endDate){
        QrandplcExample example = new QrandplcExample();
        QrandplcExample.Criteria criteria= example.createCriteria();
        criteria.andDateBetween(startDate,endDate);
        return qrandplcMapper.selectExample(example);
    }

    /**
     * 甲方需求 根据自定义sql语句查询
     * @param sql
     * @return
     */
    @Override
    public List<LinkedHashMap<String, Object>> SelectDYI(String sql) {
        return qrandplcMapper.SelectDYI(sql);
    }

    /**
     * 甲方需求 end   界面查询
     * @return
     */
    public List<Qrandplc> Query(Date startDate,Date endDate,String QRcode,String user,String state1,String type) {
        QrandplcExample example = new QrandplcExample();
        QrandplcExample.Criteria criteria= example.createCriteria();
        if (state1!=null&&!"".equals(state1)){
            criteria.andState1EqualTo(state1);
        }
        if (type!=null&&!"".equals(type)){
            criteria.andTypeEqualTo(type);
        }
        if (QRcode!=null&&!"".equals(QRcode)){
            criteria.andQrcodeEqualTo(QRcode);
            return qrandplcMapper.selectExample(example);
        }
        if (user!=null&&!"".equals(user)){
            criteria.andDateBetween(startDate,endDate).andUserEqualTo(user);
            return qrandplcMapper.selectExample(example);
        }
        criteria.andDateBetween(startDate,endDate);
        return qrandplcMapper.selectExample(example);
    }//甲方需求 end
}
