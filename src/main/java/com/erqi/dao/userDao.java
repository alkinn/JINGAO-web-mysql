package com.erqi.dao;

import com.erqi.pojo.Plcuser;

import java.util.List;

/**
 * @Author king
 * @Date 2020/7/24 15:41
 * @Version 1.0
 */
public interface userDao {

    List<Plcuser> GainUser(Plcuser user);
    int saveUser(Plcuser user);
    int delUser(Plcuser user);
    int modifyUser(Plcuser user);
    int modifyPassword(String newPassword,String oldPassword,Integer id);
}
