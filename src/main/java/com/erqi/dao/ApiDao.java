package com.erqi.dao;

import com.erqi.pojo.DailyData;
import com.erqi.pojo.Qrandplc;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @Author king
 * @Date 2020/6/28 16:30
 * @Version 1.0
 */
public interface ApiDao {
     List<DailyData> GetAllData();
     List<Qrandplc> getNo1(Date statsData, Date endData);



     //甲方需求 start
     List<Qrandplc> QueryDataByQRcode(String Qrcode);
     List<LinkedHashMap<String, Object>>  SelectDYI(String sql);
     List<Qrandplc> QueryDataByDate(Date startDate,Date endDate);
     List<Qrandplc> Query(Date startDate,Date endDate,String QRcode,String user,String state1,String type);
     //甲方需求 end
}
