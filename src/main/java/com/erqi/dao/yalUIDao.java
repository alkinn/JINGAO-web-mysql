package com.erqi.dao;

import com.erqi.pojo.Limit;
import com.erqi.pojo.Qrandplc;

import java.util.List;

/**
 * @Author king
 * @Date 2020/6/8 16:27
 * @Version 1.0
 *
 * 旧版新版分离
 */
public interface yalUIDao {

    List<Qrandplc> selectByExample(Qrandplc qrandplc, Limit limit);
    long getAllOfToday();
    long  getOfTodayRepeat();
    long  getOfTodayKaifang();
    long  getOfTodayYiti();
    int DelList(List<Integer> DList);
}
