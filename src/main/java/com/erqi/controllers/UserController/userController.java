package com.erqi.controllers.UserController;

import com.alibaba.fastjson.JSONObject;
import com.erqi.Api.BaseResponse;
import com.erqi.dao.userDao;
import com.erqi.pojo.Plcuser;
import com.erqi.pojo.Qrandplc;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author king
 * @Date 2020/7/22 15:28
 * @Version 1.0
 *
 *
 * 在这做登录相关的操作  这个包下仅做关于用户相关信息
 */
@RestController
@RequestMapping("/user")
public class userController {

    @Autowired
    userDao userDaoImpl;


    /**
     * 登录操作
     * @param plcuser
     * @return
     */
    @RequestMapping("user/login")
    public String login(@RequestBody Plcuser plcuser){
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token =new UsernamePasswordToken(plcuser.getUsername(),plcuser.getPassword());
        try{
            subject.login(token);
            return "0";
        }catch (UnknownAccountException e){//用户名不存在
            return "1";
        }catch (IncorrectCredentialsException e){//密码错误
            return "2";
        }

    }

    /**
     * 获取当前用户entry
     * @param response
     * @return
     */
    @RequestMapping(value = "getUserEntry",method = RequestMethod.POST)
    public BaseResponse getUserEntry(BaseResponse response){
        Plcuser user = (Plcuser)SecurityUtils.getSubject().getPrincipal();
        if (user!=null){
            response.setCode(0);
            response.setMsg("获取当前用户成功");
            response.setCount(0);
            response.setData(user);
            return response;
        }else {
            response.setCode(2);
            response.setMsg("获取当前用户失败");
            response.setCount(2);
            response.setData(user);
            return response;
        }
    }

    /**
     * 查询操作  附有条件查询
     * @param user
     * @return
     */
    @CrossOrigin(origins = "*",maxAge = 3600)
    @RequestMapping(value = "getAllUser",method = RequestMethod.POST)
    public BaseResponse getAllUser( Plcuser user,String searchParams){
        BaseResponse data = new BaseResponse();
        if (searchParams==null||"".equals(searchParams)) {
            List<Plcuser> plcusers = userDaoImpl.GainUser(user);
            if (plcusers == null) {
                data.setMsg("数据为空");
                data.setCode(202);
                return data;
            } else {
                data.setMsg("数据成功返回");
                data.setCode(0);
                data.setCount(plcusers.size());
                data.setData(plcusers);
                return data;
            }
        }else{
            user = JSONObject.parseObject(searchParams, Plcuser.class);

            List<Plcuser> plcusers = userDaoImpl.GainUser(user);
            if (plcusers == null) {
                data.setMsg("数据为空");
                data.setCode(202);
                return data;
            } else {
                data.setMsg("数据成功返回");
                data.setCode(0);
                data.setCount(plcusers.size());
                data.setData(plcusers);
                return data;
            }

        }
    }

    /**
     * 保存一个用户
     * @param user
     * @return
     */
    @CrossOrigin(origins = "*",maxAge = 3600)
    @RequestMapping(value = "saveUser",method = RequestMethod.POST)
    public JSONObject saveUser(@RequestBody Plcuser user){
        JSONObject data = new JSONObject();
        int i = userDaoImpl.saveUser(user);
        if (i==1){
            data.put("code",200);
            data.put("msg","保存成功");
            return data;
        }else{
            data.put("code",405);
            data.put("msg","保存失败");
            return data;
        }
    }

    @RequestMapping(value = "delUser",method = RequestMethod.POST)
    public JSONObject delUser(@RequestBody Plcuser user){
        JSONObject data = new JSONObject();
        int i = userDaoImpl.delUser(user);
        if (i==1){
            data.put("code",200);
            data.put("msg","删除成功");
            return data;
        }else{
            data.put("code",405);
            data.put("msg","删除失败");
            return data;
        }
    }


    @RequestMapping(value = "getOneUser",method = RequestMethod.GET)
    public BaseResponse getOneUser(Plcuser user){
        BaseResponse data = new BaseResponse();
        List<Plcuser> plcusers = userDaoImpl.GainUser(user);
        if (plcusers==null){
            data.setMsg("数据为空");
            data.setCode(202);
            return  data;
        }else {
            data.setMsg("数据成功返回");
            data.setCode(200);
            data.setCount(plcusers.size());
            data.setData(plcusers);
            return data;
        }
    }

    @RequestMapping(value = "modifyUser",method = RequestMethod.POST)
    public JSONObject modifyUser(@RequestBody Plcuser user){
        System.out.println("user = " + user);
        JSONObject data = new JSONObject();
        int i = userDaoImpl.modifyUser(user);
        if (i==1){
            data.put("code",200);
            data.put("msg","修改成功");
            return data;
        }else{
            data.put("code",405);
            data.put("msg","修改失败");
            return data;
        }

    }
    @RequestMapping(value = "modifyPassword",method = RequestMethod.POST)
    public JSONObject modifyPassword( String oldPassword,String newPassword,Integer id){
        JSONObject data = new JSONObject();
        if ("".equals(oldPassword)||oldPassword==null||"".equals(newPassword)||newPassword==null||id==null){
            data.put("code",405);
            data.put("msg","修改失败");
            return data;
        }else{
            int i = userDaoImpl.modifyPassword(newPassword, oldPassword, id);
            if (i==1){
                data.put("code",200);
                data.put("msg","修改成功");
                return data;
            }else if (i==2){
                data.put("code",406);
                data.put("msg","旧密码输入有误");
                return data;
            }else{
                data.put("code",405);
                data.put("msg","修改失败");
                return data;
            }

        }

    }


    @RequestMapping(value = "GetPermissions",method = RequestMethod.POST)
    public JSONObject GetPermissions(){
        JSONObject data = new JSONObject();
        data.put("code",200);
        data.put("msg","有权限");
        return data;
    }



    @CrossOrigin(origins = "*",maxAge = 3600)
    @RequestMapping(value = "test/test")
    public JSONObject saveUser1(){
        JSONObject data = new JSONObject();
            data.put("code",405);
            data.put("msg","保存失败");
            return data;
    }

}
