package com.erqi.controllers;

import com.alibaba.fastjson.JSONObject;
import com.erqi.Api.BaseResponse;
import com.erqi.dao.ApiDao;
import com.erqi.dao.plcDao;
import com.erqi.dao.yalUIDao;
import com.erqi.pojo.DailyData;
import com.erqi.pojo.Limit;
import com.erqi.pojo.Qrandplc;
import com.erqi.uitl.dates;
import org.apache.coyote.Response;
import org.apache.ibatis.jdbc.Null;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @Author king
 * @Date 2020/6/15 10:15
 * @Version 1.0
 */
@RestController
@RequestMapping("/api")
public class apiCOntroller {

    @Autowired
    yalUIDao yalUI;
    @Autowired
    plcDao plcDao;
    @Autowired
    ApiDao apiDao;

    /**
     * 数据表格
     * @param limit
     * @param searchParams
     * @param qrandplc
     * @return
     */
    @RequestMapping("table")
    public BaseResponse getMsg( Limit limit,String searchParams,Qrandplc qrandplc){
        BaseResponse baseData= new BaseResponse();
        //判断当前json是否存在数据
        if (searchParams!=null){
            //把json转为对象
            qrandplc = JSONObject.parseObject(searchParams, Qrandplc.class);
            //赋值到limit 免修改省事儿
            limit.setEndDate(qrandplc.getEndDate());
        }
        /**
         * 判断对象内是否存在数据如果  存在数据并且数据等于999 进入到筛选单个日间差
         */
        if (qrandplc.getQrcode()!=null&&qrandplc.getQrcode().equals("999")){

            BaseResponse bases= new BaseResponse();
            List<Qrandplc> no1 = apiDao.getNo1(qrandplc.getDate(),qrandplc.getEndDate());
            if (no1==null&&no1.size()>0){
                bases.setCode(500);
                bases.setMsg("这是999查询单个的数据可能没有一体机也可能没有开方机---今天没有");
            }else {
                bases.setCode(0);
                bases.setMsg("这是999查询单个的数据可能没有一体机也可能没有开方机---今天没有");
            }
            bases.setCount(no1.size());
            bases.setData(no1);
            return bases;
        }

        if (qrandplc.getDate()==null) qrandplc.setDate(dates.get0());
        if (limit.getEndDate() == null) limit.setEndDate(dates.get24());

        List<Qrandplc> qrandplcs = yalUI.selectByExample(qrandplc, limit);

        Integer selcecount = Math.toIntExact(plcDao.selcecount(qrandplc, limit));
       // Integer selcecount = qrandplcs.size();
        if (qrandplcs != null) {
            baseData.setCode(0);
            baseData.setData(qrandplcs);
        }
        if (selcecount>0){
            baseData.setCount(selcecount);
        }


        return baseData;



    }

    /**
     * 主页的30s数据 现在已经改成5s
     * @return
     */
    @RequestMapping("get30s")
    public long[] get30sMsg(){
        long[] ints =new long[4];
        ints[0]=yalUI.getAllOfToday();
        ints[1]=yalUI.getOfTodayRepeat();
        ints[2]=yalUI.getOfTodayKaifang();
        ints[3]=yalUI.getOfTodayYiti();
        return ints;
    }

    /**
     * 图表数据api
     * @return
     */
    @RequestMapping("getECharts")
    public BaseResponse getECharts(){
        BaseResponse baseData= new BaseResponse();

        List<DailyData> dailyData = apiDao.GetAllData();
        if (dailyData.size()>=6&&dailyData.size()<3){
            //当数据长度小于6 并且大于三时  给前端输出数据缺失情况。
            baseData.setCode(1); //数据长度小于6 数据缺失
            baseData.setMsg("数据缺失");
            baseData.setCount(dailyData.size());
            baseData.setData(dailyData);
            return baseData;
        }
        if (dailyData.size()<2){
            baseData.setCode(2);
            baseData.setMsg("数据错误");
            baseData.setCount(dailyData.size());
            baseData.setData(dailyData);
            return baseData;

        }
        if (dailyData==null){
            baseData.setCode(3);
            baseData.setMsg("数据为空");
            baseData.setCount(dailyData.size());
            baseData.setData(dailyData);
            return baseData;

        }
        baseData.setCode(0);
        baseData.setMsg("数据读取成功");
        baseData.setCount(dailyData.size());
        baseData.setData(dailyData);
        return baseData;

    }



    /**
     * 甲方需求 对公 Api接口 根据单晶号查询
     * @param QRcode
     * @return
     */

    @RequestMapping("QueryByQRcode")
    @CrossOrigin(origins = "*",maxAge = 3600)
    public BaseResponse QueryByQRcode(String QRcode){
        BaseResponse baseData= new BaseResponse();
        List<Qrandplc> qrandplcs = apiDao.QueryDataByQRcode(QRcode);
        if (qrandplcs==null){
            baseData.setCode(2);
            baseData.setMsg("数据错误，检查您的单晶号填写是否正确");
            baseData.setData(null);
            baseData.setCount(0);
            return baseData;
        } else
        if (qrandplcs.size()<=0){
            baseData.setCode(3);
            baseData.setMsg("数据长度小于0，您的单晶号可能有误");
            baseData.setData(qrandplcs);
            baseData.setCount(qrandplcs.size());
            return baseData;
        }else {
            baseData.setCode(0);
            baseData.setMsg("查询成功");
            baseData.setData(qrandplcs);
            baseData.setCount(qrandplcs.size());
            return baseData;
        }
    }


    /**
     * 甲方需求 对公 Api接口  自定义sql语句查询
     * @param sql
     * @return
     */
    @RequestMapping("QueryBySql")
    @CrossOrigin(origins = "*",maxAge = 3600)
    public BaseResponse  QueryDYI(String sql){
        BaseResponse baseData= new BaseResponse();
        List<LinkedHashMap<String, Object>> selectDYI=null;
        try{
            selectDYI = apiDao.SelectDYI(sql);
            baseData.setCode(0);
            baseData.setMsg("查询成功");
            baseData.setData(selectDYI);
            //baseData.setData(JSON.toJSONStringWithDateFormat(selectDYI,"yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteDateUseDateFormat));

            baseData.setCount(selectDYI.size());
            return baseData;

        }catch (BadSqlGrammarException e){
            baseData.setCode(2);
            baseData.setMsg("查询错误--异常位置"+e.getSQLException().getMessage());
            baseData.setData(e.getMessage());
            baseData.setCount(0);
            return baseData;
        }
    }
    /**
     * 甲方需求 对公 Api接口  根据时间段查询
     * @param startDate
     * @return
     */
    @RequestMapping("QueryByDate")
    @CrossOrigin(origins = "*",maxAge = 3600)
    public BaseResponse QueryByDate(Date startDate,Date endDate){
        BaseResponse baseData= new BaseResponse();
        List<Qrandplc> qrandplcs = apiDao.QueryDataByDate(startDate,endDate);
        if (qrandplcs==null){
            baseData.setCode(2);
            baseData.setMsg("数据错误，检查您的单晶号填写是否正确");
            baseData.setData(null);
            baseData.setCount(0);
            return baseData;
        } else
        if (qrandplcs.size()<=0){
            baseData.setCode(3);
            baseData.setMsg("数据长度小于0，您的单晶号可能有误");
            baseData.setData(qrandplcs);
            baseData.setCount(qrandplcs.size());
            return baseData;
        }else {
            baseData.setCode(0);
            baseData.setMsg("查询成功");
            baseData.setData(qrandplcs);
            baseData.setCount(qrandplcs.size());
            return baseData;
        }

    }


    /**
     * 界面查询     简易
     * @param startDate
     * @param endDate
     * @param QRcode
     * @param user
     * @param sql
     * @return
     */
    @RequestMapping(value = "Querys",method = RequestMethod.GET)
    @CrossOrigin(origins = "*",maxAge = 3600)
    public BaseResponse Querys(Date startDate,Date endDate,String QRcode,String user,String sql,String state1,String type){
        System.out.println("startDate = " + startDate + ", endDate = " + endDate + ", QRcode = " + QRcode + ", user = " + user + ", sql = " + sql + ", state1 = " + state1 + ", type = " + type);
        BaseResponse baseData= new BaseResponse();
        if (sql!=null&&sql.length()>1){
            List<LinkedHashMap<String, Object>> selectDYI=null;
            try{
                selectDYI = apiDao.SelectDYI(sql);
                baseData.setCode(0);
                baseData.setMsg("查询成功");
                baseData.setData(selectDYI);
                //baseData.setData(JSON.toJSONStringWithDateFormat(selectDYI,"yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteDateUseDateFormat));

                baseData.setCount(selectDYI.size());
                return baseData;

            }catch (BadSqlGrammarException e){
                baseData.setCode(2);
                baseData.setMsg("查询错误--异常位置"+e.getSQLException().getMessage());
                baseData.setData(e.getMessage());
                baseData.setCount(0);
                return baseData;
            }
        }else{
            List<Qrandplc> qrandplcs = apiDao.Query(startDate,endDate,QRcode,user,state1,type);
            if (qrandplcs==null){
                baseData.setCode(2);
                baseData.setMsg("数据错误，检查您的单晶号或者时间填写是否正确");
                baseData.setData(null);
                baseData.setCount(0);
                return baseData;
            } else
            if (qrandplcs.size()<=0){
                baseData.setCode(3);
                baseData.setMsg("数据长度小于0，您的单晶号或者时间可能有误");
                baseData.setData(qrandplcs);
                baseData.setCount(qrandplcs.size());
                return baseData;
            }else {
                baseData.setCode(0);
                baseData.setMsg("查询成功");
                baseData.setData(qrandplcs);
                baseData.setCount(qrandplcs.size());
                return baseData;
            }
        }
    }
























}
