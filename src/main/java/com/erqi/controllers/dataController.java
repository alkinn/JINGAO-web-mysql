package com.erqi.controllers;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.erqi.Api.BaseResponse;
import com.erqi.dao.plcDao;
import com.erqi.dao.yalUIDao;
import com.erqi.pojo.Limit;
import com.erqi.pojo.Qrandplc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author king
 * @Date 2020/6/5 11:43
 * @Version 1.0
 */
@RestController
@RequestMapping("/data")
public class dataController {

    @Autowired
    plcDao plcDao;
    @Autowired
    yalUIDao yalUIDao;

    /**
     * 查询所有
     * @return kong
     */
    @RequestMapping("getAll")
    public String getAll(){
        List<Qrandplc> qrandplcs = plcDao.queryAll();
        return JSON.toJSONStringWithDateFormat(qrandplcs,"yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteDateUseDateFormat);

    }

    /**
     * 根据分页查询
     * @param qrandplc data
     * @param limit  num
     * @return list
     */
    @RequestMapping("getallfopage")
    public String getAllFoPage(Qrandplc qrandplc, Limit limit){
        System.out.println("qrandplc = " + qrandplc + ", limit = " + limit);
        List<Qrandplc> qrandplcs = plcDao.selectByExample(qrandplc, limit);
       return JSON.toJSONStringWithDateFormat(qrandplcs,"yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteDateUseDateFormat);
    }

    /**
     * 查询行数
     * @param qrandplc data
     * @param limit num
     * @return int
     */
    @RequestMapping("count")
    public int getCount(Qrandplc qrandplc, Limit limit){
        System.out.println("qrandplc = " + qrandplc + ", limit = " + limit);
        return Math.toIntExact(plcDao.selcecount(qrandplc, limit));
    }

    /**
     * 插入一条数
     * @param qrandplc data
     * @return int
     */
    @RequestMapping("setData")
    public int setQrandplc(Qrandplc qrandplc){
        return  plcDao.AddQrandplc(qrandplc);
    }

    /**
     * 删除这里的删除不是真正意义上的删除而是吧 状态改为删除状态
     * @param delList
     * @return
     */
    @RequestMapping("dellist")
    public int deleteList(@RequestParam(value = "delList",required = true)List<Integer> delList){
        delList.forEach(System.out::println);
        return plcDao.DelList(delList);
    }

    @RequestMapping("uiget")
    public BaseResponse geyYalUi(Qrandplc qrandplc, Limit limit){

        List<Qrandplc> qrandplcs = yalUIDao.selectByExample(qrandplc, limit);

        Integer selcecount = Math.toIntExact(plcDao.selcecount(qrandplc, limit));
        BaseResponse response = new BaseResponse();
        if (qrandplcs != null) {
            response.setCode(0);
            response.setData(qrandplcs);
        }
        response.setCount(selcecount);

        return response;
    }
}
