package com.erqi.controllers;

import com.erqi.Api.BaseResponse;
import com.erqi.pojo.Plcuser;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @Author king
 * @Date 2020/6/5 13:53
 * @Version 1.0
 * 所有的跳转页面都在这。
 */
@Controller
public class indexController {

    //主页
    @RequestMapping({"/index","/"})
    public String index(){
        return "index";
    }

    //测试案列
    @RequestMapping("/test")
    public String data_listindex(){
        return "data_list";
    }

    @RequestMapping("/tologin")
    public String toLogin(){
        return "page/login-2";
    }


    @RequestMapping("/in")
    public String toin(HttpSession session){

        Plcuser plcuser = (Plcuser)session.getAttribute("loginUser");
        System.out.println(plcuser.toString());
        return "page/user-setting";
    }

    @RequestMapping("/666")
    @ResponseBody
    public String logout(HttpSession session){

       SecurityUtils.getSubject().logout();

        return "登出成功";
    }

    @RequestMapping("/noauth")
    @ResponseBody
    public BaseResponse noauth(HttpSession session){
        BaseResponse base = new BaseResponse();
        base.setCount(0);
        base.setMsg("您暂无权限");
        base.setCode(407);
        base.setData(null);
        return base;
    }




    @RequestMapping("/nologin")
    @ResponseBody
    public BaseResponse nologin(HttpSession session){
        BaseResponse base = new BaseResponse();
        base.setCount(0);
        base.setMsg("未登录");
        base.setCode(401);
        base.setData(null);
        return base;
    }
    @RequestMapping("/pp/ps")
    @ResponseBody
    public String get1(HttpSession session){
        return "ps";
    }
    @RequestMapping("/pp/pp")
    @ResponseBody
    public String pp(HttpSession session){
        return "pp";
    }
}


