package com.erqi.controllers.Shiro;

import com.alibaba.fastjson.JSONObject;
import com.erqi.Api.BaseResponse;
import com.erqi.dao.ApiDao;
import com.erqi.dao.plcDao;
import com.erqi.dao.yalUIDao;
import com.erqi.pojo.Qrandplc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author king
 * @Date 2020/7/23 13:08
 * @Version 1.0
 */
@RestController
@RequestMapping("/api/shrio")
public class ShiroController {

    @Autowired
    yalUIDao yalUI;
    @Autowired
    plcDao plcDao;
    @Autowired
    ApiDao apiDao;



    /**
     * 删除这里的删除不是真正意义上的删除而是吧 状态改为删除状态
     * @param
     * @return
     */
    @RequestMapping("dellist")
    public Integer deleteList(@RequestBody List<Integer> delList){
        return plcDao.DelList(delList);
    }


    /**
     * 插入一条数
     * @param qrandplc data
     * @return int
     */
    @RequestMapping("setData")
    public int setQrandplc(Qrandplc qrandplc){
        return  plcDao.AddQrandplc(qrandplc);
    }


    @RequestMapping(value = "getOneDataById",method = RequestMethod.GET)
    public BaseResponse getData(Integer id){
        BaseResponse data = new BaseResponse();
        if (id!=null&&id>0){
            Qrandplc qrandplc = plcDao.QueryPlcById(id);
            if (qrandplc!=null){
                data.setCode(200);//成功
                data.setMsg("成功查到数据");
                data.setData(qrandplc);
                return data;
            }else{
                data.setCode(202);//成功
                data.setMsg("数据为空");
                data.setData(qrandplc);
                return data;
            }
        }else{
            data.setCode(405);//成功
            data.setMsg("查询失败");
            return data;
        }

    }


    @RequestMapping(value = "modifyData",method = RequestMethod.POST)
    public JSONObject modifyData(@RequestBody Qrandplc qrandplc){
        JSONObject data = new JSONObject();
        if (qrandplc!=null && 0<qrandplc.getId()){
            int i = plcDao.modifyQeandplcData(qrandplc);
            if (i==1){
                data.put("code",200);
                data.put("msg","修改成功");
                return data;
            }else{
                data.put("code",405);
                data.put("msg","修改失败");
                return data;
            }
        }else{
            data.put("code",405);
            data.put("msg","修改失败");
            return data;
        }
    }


    @RequestMapping(value = "GetPermissions",method = RequestMethod.POST)
    public JSONObject GetPermissions(){
        JSONObject data = new JSONObject();
        data.put("code",200);
        data.put("msg","有权限");
        return data;
    }

}
