package com.erqi;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.erqi.dao.ApiDao;
import com.erqi.dao.plcDao;
import com.erqi.mapper.DailyDataMapper;
import com.erqi.mapper.PlcuserMapper;
import com.erqi.mapper.QrandplcMapper;
import com.erqi.pojo.*;
import com.erqi.uitl.dates;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ErqiApplicationTests {

    @Autowired
    plcDao plc;

    @Autowired
    QrandplcMapper plcs;

    @Autowired
    ApiDao apiDao;

    @Autowired
    DailyDataMapper dataMapper;


    @Autowired
    PlcuserMapper userMapper;

    @Test
    public void contextLoads() {

        Qrandplc qrandplc = plc.QueryPlcById(1);
        System.out.println("qrandplc = " + qrandplc);
    }

    @Test
    public  void testin(){
        Qrandplc plCdata = new Qrandplc();
        plCdata.setDate(new Date());
        plCdata.setType("一体机");
        plCdata.setNumRobot("1");
        plCdata.setQrcode("BT20064110151-1");
        plCdata.setNumMachine("30");
        plCdata.setWorkshop("2");
        int i = plc.insertSelective(plCdata);
        System.out.println("i = " + i);
    }


    @Test
    public void Testquery(){

        QrandplcExample qrandplcExample = new QrandplcExample();
        qrandplcExample.setLeftLimit(9);
        qrandplcExample.setLimitSize(10);

        qrandplcExample.createCriteria().andDateBetween(new Date(),new Date()).andNumMachineEqualTo("10");
        qrandplcExample.or().andTypeEqualTo("一体机");

        List<Qrandplc> qrandplcs = plcs.selectByExample(qrandplcExample);
        qrandplcs.forEach(System.out::println);
    }

    @Test
    public void Testcount(){

        QrandplcExample qrandplcExample = new QrandplcExample();
        qrandplcExample.setLeftLimit(9);
        qrandplcExample.setLimitSize(10);

        qrandplcExample.createCriteria().andNumMachineEqualTo("30");


        int qrandplcs = (int) plcs.countByExample(qrandplcExample);
        System.out.println("qrandplcs = " + qrandplcs);
    }

    

    @Test
    public void TestapiDao(){
        apiDao.GetAllData().forEach(System.out::println);
    }
    @Test
    public void test000000(){
        long[] kai = plc.getKai();
        for (long s :
        kai) {
            System.out.println("s = " + s);
        }
        dataMapper.insert(new DailyData(new Date(),(int)kai[0],(int)kai[1],(int)kai[2]));



    }



    @Test
    public void getNo1(){

          apiDao.getNo1(dates.get0(),dates.get24()).forEach(System.out::println);

    }



    @Test
    public void contextLoadsBy() {

        List<Qrandplc> qrandplcs = apiDao.QueryDataByQRcode("BT20064110151-1");
        qrandplcs.forEach(System.out::println);
    }


    @Test
    public void TestSelectDYI(){
        List<LinkedHashMap<String, Object>> qrandplcs = plcs.SelectDYI("SELECT * FROM qrandplc WHERE QRcode='单晶号'");
        String s = JSON.toJSONStringWithDateFormat(qrandplcs, "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteDateUseDateFormat);
        System.out.println("s = " + s);
        qrandplcs.forEach(System.out::println);
    }
    @Test
    public void TestSelectDYI2(){

        List<LinkedHashMap<String, Object>> qrandplcs = apiDao.SelectDYI("SELECT * F ROM qrandplc WHERE QRcode='单晶号'");
        if (qrandplcs!=null) {
            String s = JSON.toJSONStringWithDateFormat(qrandplcs, "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteDateUseDateFormat);
            System.out.println("s = " + s);
            System.out.println(qrandplcs.size() + "=长度");
            qrandplcs.forEach(System.out::println);
        }else{
            System.out.println("他是个空的");
        }
    }
    @Test
    public void TestSelectDYI3(){
        QrandplcExample examples =new QrandplcExample();

        examples.createCriteria().andUserEqualTo("雷霆").andState1EqualTo("入库成功").andTypeEqualTo("开方机");
        List<Qrandplc> qrandplcs = plcs.NEWselectByExample(examples);
        System.out.println(qrandplcs.size());
        qrandplcs.forEach(System.out::println);
        System.out.println("-**********/*/*/");

        Qrandplc qrandplc =new Qrandplc();
        qrandplc.setDate(dates.getPastDate(100,24));
        qrandplc.setUser("雷霆");
        Limit limit = new Limit();
        limit.setEndDate(new Date());
        Long selcecount = plc.selcecount(qrandplc, limit);
        System.out.println("selcecount = " + selcecount);

    }

    @Test
    public void Test1(){
        Plcuser admin = userMapper.selectByUserName("admin");
        System.out.println("admin = " + admin);
    }

    @Test
    public void Test2(){
        DailyData dailyData = new DailyData();
        dailyData.setAlls(11);
        dailyData.setDate(new Date());
        dailyData.setK1(1);
        dailyData.setT1(1);
        dataMapper.insert(dailyData);
    }


    @Test
    public void Test3(){
        QrandplcExample example =new QrandplcExample();
        example.createCriteria().andDateBetween(dates.getPastDate(1,0),dates.getPastDate(1,24))
                .andNumRobotEqualTo("2")
                .andTypeEqualTo("开方机").andState1EqualTo("入库成功");
        long l = plcs.countByExample(example);
        System.out.println("l = " + l);
    }
}
